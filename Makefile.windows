#############################################################
#
# Root Level Makefile
#
# (c) by CHERTS <sleuthhound@gmail.com>
#
#############################################################

BUILD_BASE	= build
FW_BASE		= firmware

FIND ?= find
FLASH_OPTS ?= -fs 4m

# Base directory for the compiler
XTENSA_TOOLS_ROOT ?= c:/Espressif/xtensa-lx106-elf/bin

# base directory of the ESP8266 SDK package, absolute
SDK_BASE	?= c:/Espressif/ESP8266_SDK
SDK_TOOLS	?= c:/Espressif/utils

FW_TOOL       ?= C:/esptools/esptool2.exe
FW_SECTS = .text .data .rodata
FW_USER_ARGS = -quiet -bin -boot2 -512

#Extra Tensilica includes from the ESS VM
SDK_EXTRA_INCLUDES ?= c:/Espressif/ESP8266_SDK/include
SDK_EXTRA_LIBS ?= c:/Espressif/ESP8266_SDK/lib

# esptool path and port
ESPTOOL ?=c:/espressif/utils/esptool.exe
ESPPORT ?=COM6
ESPBAUD ?=115200

#Position and maximum length of espfs in flash memory
ESPFS_POS  = 0x70000
ESPFS_SIZE = 0x08000

#Static gzipping is disabled by default.
GZIP_COMPRESSION ?= no

#If USE_HEATSHRINK is set to "yes" then the espfs files will be compressed with Heatshrink and decompressed
#on the fly while reading the file. Because the decompression is done in the esp8266, it does not require
#any support in the browser.
USE_HEATSHRINK ?= yes

# name for the target project
TARGET = app

# which modules (subdirectories) of the project to include in compiling
MODULES		= driver mqtt user easygpio

EXTRA_INCDIR	= include $(SDK_BASE)/../include \
		. \
		lib/heatshrink/ \
		mqtt \
		$(SDK_EXTRA_INCLUDES) 

# libraries used in this project, mainly provided by the SDK
LIBS = c gcc hal phy pp net80211 lwip wpa pwm upgrade main ssl

CFLAGS = -w -Os -ggdb -std=c99 -Wpointer-arith -Wundef -Wall -Wl,-EL -fno-inline-functions \
		-nostdlib -mlongcalls -mtext-section-literals  -D__ets__ -DICACHE_FLASH -D_STDINT_H \
		-Wno-address -DESPFS_POS=$(ESPFS_POS) -DESPFS_SIZE=$(ESPFS_SIZE)

# linker flags used to generate the main object file
LDFLAGS = -nostdlib -Wl,--no-check-sections -u call_user_start -Wl,-static 

# linker script used for the above linker step
LD_SCRIPT = eagle.app.v6.ld

# various paths from the SDK used in this project
SDK_LIBDIR	= lib
SDK_LDDIR	= ld
SDK_INCDIR	= include include/json

# select which tools to use as compiler, librarian and linker
CC		:= $(XTENSA_TOOLS_ROOT)/xtensa-lx106-elf-gcc
AR		:= $(XTENSA_TOOLS_ROOT)/xtensa-lx106-elf-ar
LD		:= $(XTENSA_TOOLS_ROOT)/xtensa-lx106-elf-gcc
OBJCOPY := $(XTENSA_TOOLS_ROOT)/xtensa-lx106-elf-objcopy
OBJDUMP := $(XTENSA_TOOLS_ROOT)/xtensa-lx106-elf-objdump

# no user configurable options below here
SRC_DIR		:= $(MODULES)
BUILD_DIR	:= $(addprefix $(BUILD_BASE)/,$(MODULES))

SDK_LIBDIR	:= $(addprefix $(SDK_BASE)/,$(SDK_LIBDIR))
SDK_INCDIR	:= $(addprefix -I$(SDK_BASE)/,$(SDK_INCDIR))

SRC		:= $(foreach sdir,$(SRC_DIR),$(wildcard $(sdir)/*.c))
OBJ		:= $(patsubst %.c,$(BUILD_BASE)/%.o,$(SRC))
LIBS		:= $(addprefix -l,$(LIBS))
APP_AR		:= $(addprefix $(BUILD_BASE)/,$(TARGET)_app.a)
TARGET_OUT	:= $(addprefix $(BUILD_BASE)/,$(TARGET).out)

LD_SCRIPT	:= $(addprefix -T,$(LD_SCRIPT))

INCDIR	:= $(addprefix -I,$(SRC_DIR))
EXTRA_INCDIR	:= $(addprefix -I,$(EXTRA_INCDIR))
MODULE_INCDIR	:= $(addsuffix /include,$(INCDIR))

ifeq ("$(GZIP_COMPRESSION)","yes")
CFLAGS		+= -DGZIP_COMPRESSION
endif

ifeq ("$(USE_HEATSHRINK)","yes")
CFLAGS		+= -DESPFS_HEATSHRINK
endif

vpath %.c $(SRC_DIR)

define compile-objects
$1/%.o: %.c
	@echo "CC $$<"
	@$(CC) $(INCDIR) $(MODULE_INCDIR) $(EXTRA_INCDIR) $(SDK_INCDIR) $(CFLAGS)  -c $$< -o $$@
endef

.PHONY: all clean flash flashinit flashonefile htmlfiles 

all: $(BUILD_DIR) $(FW_BASE) $(FW_BASE)/$(TARGET).bin $(FW_BASE)/webpages.espfs
    
$(TARGET_OUT): $(APP_AR)
	@echo "LD $@"
	@$(LD) -L$(SDK_LIBDIR) $(LD_SCRIPT) $(LDFLAGS) -Wl,--start-group $(LIBS) $(APP_AR) -Wl,--end-group -o $@
	@echo "------------------------------------------------------------------------------"
	@echo "Section info:"
	@$(OBJDUMP) -h -j .data -j .rodata -j .bss -j .text -j .irom0.text $@
	@echo "------------------------------------------------------------------------------"

$(FW_BASE)/$(TARGET).bin: $(TARGET_OUT)
	@echo "FW $@"
	@$(FW_TOOL) $(FW_USER_ARGS) $(TARGET_OUT) $@ $(FW_SECTS)

$(FW_BASE)/webpages.espfs: $(FW_BASE)
	@echo "FS $@"
	cd html; $(FIND) | ../mkespfsimage/mkespfsimage.exe > ../$@; cd ..

$(APP_AR): $(OBJ)
	@echo "AR $@"
	@$(AR) cru $@ $^

$(BUILD_DIR):
	@mkdir -p $@

$(FW_BASE):
	@mkdir -p $@

flash: all
	$(ESPTOOL) -p $(ESPPORT) -b $(ESPBAUD) write_flash $(FLASH_OPTS) --flash_freq 80m 0x00000 rboot.bin 0x02000 $(FW_BASE)/$(TARGET).bin $(ESPFS_POS) $(FW_BASE)/webpages.espfs

rebuild: clean all

clean:
	@echo "RM $(BUILD_BASE)"
	@rm -rf $(BUILD_BASE)
	@echo "RM $(FW_BASE)"
	@rm -rf $(FW_BASE)

$(foreach bdir,$(BUILD_DIR),$(eval $(call compile-objects,$(bdir))))

